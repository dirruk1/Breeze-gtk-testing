#!/bin/bash

if [ -d ./Breeze ]
  then rm -rf ./Breeze
fi

mkdir ./Breeze

python render_assets.py -f -i ./Breeze.colors -o .

mkdir ./Breeze/gtk-3.18
mkdir ./Breeze/gtk-3.20

cp ./Breeze/gtk-3.0/gtk.css ./Breeze/gtk-3.18/gtk.css
cp ./Breeze/gtk-3.0/gtk.css ./Breeze/gtk-3.20/gtk.css
rm ./Breeze/gtk-3.0/gtk.css

 
sassc src/gtk318/gtk-main.scss ./Breeze/gtk-3.18/gtk-main.css
sassc src/gtk320/gtk-main.scss ./Breeze/gtk-3.20/gtk-main.css

cp -R src/gtk2/widgets Breeze/gtk-2.0/

#if [ -d /usr/share/themes/Breeze ]
#  then sudo rm -rf /usr/share/themes/Breeze
#fi

#sudo mv -f ./Breeze /usr/share/themes/Breeze
        
