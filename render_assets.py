#! /usr/bin/env python

import cairo
import colorsys
import math
import os
import errno
import sys
import argparse

class Color():
    intensity_effect = 2
    intensity_amount = 0.1
    color_effect = 0
    color_amount = 0
    effect_color = (0.22,0.22,0.22)
    contrast_effect = 1
    contrast_amount = 0.65
    window_color = (0.74,0.76,0.78)
    def __init__(self, color1, color2=None, amount=0):
        r = float(color1.split(',')[0])
        g = float(color1.split(',')[1])
        b = float(color1.split(',')[2])
        if not color2 == None:
            r = r * amount + float(color2.split(',')[0]) * (1 - amount)
            g = g * amount + float(color2.split(',')[1]) * (1 - amount)
            b = b * amount + float(color2.split(',')[2]) * (1 - amount)

        self.rgb = (r/255,g/255,b/255)
        self.html = '#{0:02x}{1:02x}{2:02x}'.format(int(r),int(g),int(b))
        _dis = self._color_effect(self._intensity_effect(self.rgb))
        _dis_alpha = self._contrast_effect(self.rgb)
        self.disabled = mix(_dis, Color.window_color, _dis_alpha)

    def _desaturate(self,color,amount):
        h,s,v = colorsys.rgb_to_hsv(color[0],color[1],color[2])
        s = min(s * (1 - amount),1)
        r,g,b = colorsys.hsv_to_rgb(h,s,v)
        return (r,g,b)

    def _intensity_effect(self,color):
        if Color.intensity_effect == 0:
            (r,g,b) = color
        elif Color.intensity_effect == 1:
            if Color.intensity_amount >= 0:
                (r,g,b) = mix((1.0,1.0,1.0),color,Color.intensity_amount)
            else:
                (r,g,b) = mix((0.0,0.0,0.0),color,Color.intensity_amount)
        elif Color.intensity_effect == 2:
            (r,g,b) = darker(color,Color.intensity_amount)
        elif Color.intensity_effect == 3:
            (r,g,b) = lighter(color,Color.intensity_amount)
        return (r,g,b)

    def _color_effect(self,color):
        if Color.color_effect == 0:
            (r,g,b) = color
        elif Color.color_effect == 1:
            (r,g,b) = self._desaturate(color,Color.color_amount)
        else:
            (r,g,b) = mix(Color.effect_color,color,Color.color_amount)
        return (r,g,b)

    def _contrast_effect(self,color):
        if Color.contrast_effect == 0:
            return 1.0
        else:
            return 1.0 - Color.contrast_amount

def lighter(color,amount):
    h,s,v = colorsys.rgb_to_hsv(color[0],color[1],color[2])
    v = min((1+amount)*v,1)
    r,g,b = colorsys.hsv_to_rgb(h,s,v)
    return (r,g,b)

def darker(color,amount):
    h,s,v = colorsys.rgb_to_hsv(color[0],color[1],color[2])
    if amount == -1:
        v = 1
    else:
        v = min(v/(1+amount),1)
    r,g,b = colorsys.hsv_to_rgb(h,s,v)
    return (r,g,b)

def mix(color, mix_color, amount):
    r = color[0] * amount + mix_color[0] * (1 - amount)
    g = color[1] * amount + mix_color[1] * (1 - amount)
    b = color[2] * amount + mix_color[2] * (1 - amount)
    return (r,g,b)

def gradient(color, state,x0=1,y0=1,x1=1,y1=19):
    if state == 'active' or state == 'focus':
        stop1=lighter(color,0.03)
        stop2=darker(color,0.10)
    else:
        stop1=lighter(color,0.01)
        stop2=darker(color,0.03)
    linear = cairo.LinearGradient(1, 1, 1, 19)
    linear.add_color_stop_rgb(0.0,stop1[0],stop1[1],stop1[2])
    linear.add_color_stop_rgb(1.0,stop2[0],stop2[1],stop2[2])
    return linear

class Drawing():
    assetsdir='./'
    def __init__(self,width,height,scl=1, rotation=0):
        self.surface = cairo.ImageSurface(cairo.FORMAT_ARGB32, scl*width, scl*height)
        cr = self.cr = cairo.Context(self.surface)
        self.w=width; self.h=height
        if rotation != 0:
            cr.translate(scl*width/2,scl*height/2)
            cr.rotate(rotation*math.pi/2)
            cr.translate(-scl*width/2,-scl*height/2)
        cr.scale(scl,scl)

    def rectangle(self,color,x,y,width,height,gradient=False):
        self.cr.rectangle(x,y,width,height)
        if gradient:
            self.cr.set_source(color)
        elif color == None:
            self.cr.set_operator(cairo.OPERATOR_CLEAR)
        else:
            self.cr.set_source_rgb(color[0],color[1],color[2])
        self.cr.fill()

    def rounded_rectangle(self, color, w, h, x, y, r, gradient=False, alpha=1):
        if isinstance(r, tuple):
            r1=r[0]
            r2=r[1]
            r3=r[2]
            r4=r[3]
        else:
            r1=r2=r3=r4=r
            
        self.cr.new_sub_path()
        self.cr.arc(x + w - r1, y + r1, r1, -math.pi/2, 0)
        self.cr.arc(x + w - r2, y + h - r2, r2, 0, math.pi/2)
        self.cr.arc(x + r3, y + h - r3, r3, math.pi/2, math.pi)
        self.cr.arc(x + r4, y + r4, r4, math.pi, 3*math.pi/2)
        self.cr.close_path()
        if gradient:
            self.cr.set_source(color)
        elif color == None:
            self.cr.set_operator(cairo.OPERATOR_CLEAR)
        elif color == 'shadow':
            self.cr.set_source_rgba(0.0,0.0,0.0, 0.075)
        else:
            self.cr.set_source_rgba(color[0],color[1],color[2],alpha)
        self.cr.fill()

    def rounded_triangle(self, color, width, height, x, y, radius):
        self.cr.new_sub_path()
        self.cr.move_to(x + width, y)
        self.cr.line_to(x + width, y + height - radius)
        self.cr.arc(x + width - radius, y + height - radius,radius, 0, math.pi/2)
        self.cr.line_to(x, y + height)
        self.cr.close_path()
        self.cr.set_source_rgb(color[0],color[1],color[2])
        self.cr.fill()

    def circle(self, color, x, y, radius, gradient=False):
        self.cr.new_sub_path()
        self.cr.arc(x, y, radius, 0, 2*math.pi)
        self.cr.close_path()
        if gradient:
            self.cr.set_source(color)
        elif color == None:
            self.cr.set_operator(cairo.OPERATOR_CLEAR)
        elif color == 'shadow':
            self.cr.set_source_rgba(0.0,0.0,0.0, 0.15)
        else:
            self.cr.set_source_rgb(color[0],color[1],color[2])
        self.cr.fill()

    def half_circle(self, color, x, y, radius):
        self.cr.new_sub_path()
        self.cr.arc(x, y, radius, -math.pi/4, 3*math.pi/4)
        self.cr.close_path()
        self.cr.set_source_rgb(color[0],color[1],color[2])
        self.cr.fill()

    def arrow(self, color, alpha=1.0, shiftx=0, shifty=1):
        self.cr.new_sub_path()
        self.cr.move_to(shiftx + 2,shifty + 7)
        self.cr.line_to(shiftx + 6,shifty + 3)
        self.cr.line_to(shiftx + 10,shifty + 7)
        self.cr.set_source_rgb(color[0],color[1],color[2])
        self.cr.set_line_width(1.0)
        self.cr.stroke()

    def arrow_small(self, color):
        self.cr.new_sub_path()
        self.cr.move_to(1,6)
        self.cr.line_to(4,3)
        self.cr.line_to(7,6)
        self.cr.set_source_rgba(color[0],color[1],color[2])
        self.cr.set_line_width(1.0)
        self.cr.stroke()

    def minimize(self,color=None,x=0,y=0):
        self.cr.move_to(x+4,y+7)
        self.cr.line_to(x+9,y+12)
        self.cr.line_to(x+14,y+7)
        if color == None:
            self.cr.set_operator(cairo.OPERATOR_CLEAR)
        else:
            self.cr.set_source_rgb(color[0],color[1],color[2])
        self.cr.set_line_width(1.0)
        self.cr.stroke()

    def maximize(self,color=None,shifty=0,x=0,y=0):
        self.cr.move_to(x+4,y+11+shifty)
        self.cr.line_to(x+9,y+6+shifty)
        self.cr.line_to(x+14,y+11+shifty)
        if color == None:
            self.cr.set_operator(cairo.OPERATOR_CLEAR)
        else:
            self.cr.set_source_rgb(color[0],color[1],color[2])
        self.cr.set_line_width(1.0)
        self.cr.stroke()

    def maximize_maximized(self,color=None,x=0,y=0):
        self.cr.move_to(x+4.5,y+9)
        self.cr.line_to(x+9,y+4.5)
        self.cr.line_to(x+13.5,y+9)
        self.cr.line_to(x+9,y+13.5)
        self.cr.close_path()
        if color == None:
            self.cr.set_operator(cairo.OPERATOR_CLEAR)
        else:
            self.cr.set_source_rgb(color[0],color[1],color[2])
        self.cr.set_line_width(1.0)
        self.cr.stroke()

    def close(self,color=None,x=0,y=0):
        self.cr.move_to(x+5,y+5)
        self.cr.line_to(x+13,y+13)
        self.cr.move_to(x+13,y+5)
        self.cr.line_to(x+5,y+13)
        if color == None:
            self.cr.set_operator(cairo.OPERATOR_CLEAR)
        else:
            self.cr.set_source_rgb(color[0],color[1],color[2])
        self.cr.set_line_width(1.0)
        self.cr.stroke()

    def save(self,filename,scl=1):
        if scl == 1:
            self.surface.write_to_png(self.assetsdir+filename+'.png')
        else:
            self.surface.write_to_png(self.assetsdir+filename+'@2.png')

class Button(Drawing):
    scl=1
    def __init__(self, colors):
        self.name='button'
        self.render(colors)

    def _bg(self):
        self.rectangle(Color.window_color,0,0,20,20)

    def _shadow(self):
        self.rounded_rectangle('shadow',18,18,2,2,3)
        self.rounded_rectangle('shadow',18,18,1.3,1.3,3)

    def _widget_bg(self, x, color1, color2, border=0, gradient=False):
        if border > 0:
            self.rounded_rectangle(color1,18,18,x,x,3)
        self.rounded_rectangle(color2,18-2*border,18-2*border,x+border,x+border,3-border,gradient)

    def _widget(self, state, color1, color2):
        self._shadow()
        if state is 'active':
            color2=gradient(color1,state)
            self._widget_bg(2, color1, color2, 0, True)
        else:
            color2=gradient(color2,state)
            self._widget_bg(1, color1, color2, 1, True)

    def draw_asset(self, state, color1, color2, rotation=0, w=20, h=20):
        Drawing.__init__(self,w,h,self.scl,rotation)
        self._widget(state, color1, color2)
        self.save(self.name+'-'+state,self.scl)

    def render(self, colors, rotation=0, w=20, h=20):
        self.draw_asset('normal', colors['border'].rgb, colors['bg'].rgb, rotation, w, h)
        self.draw_asset('hover',  colors['hover'].rgb, colors['bg'].rgb, rotation, w, h)
        self.draw_asset('active', colors['hover'].rgb, colors['bg'].rgb, rotation, w, h)
        self.draw_asset('disabled', colors['border'].disabled, colors['bg'].disabled, rotation, w, h)

class ToolButton(Button):
    def __init__(self, colors):
        self.name='toolbutton'
        self.render(colors)

    def _widget(self, state, color1, color2):
        if state in ['normal','disabled']:
            return
        if state is 'hover':
            self._widget_bg(1,color1,color2,1)
        if state is 'active':
            color2=gradient(color1,state)
            self._widget_bg(1,color1,color2,0,True)
        if state is 'toggled':
            self._widget_bg(1,color1, color1,0)

    def render(self, colors):
        Button.render(self,colors)
        self.draw_asset('toggled', colors['border'].rgb, colors['bg'].rgb)

class ToggleButton(Button):
    def __init__(self, colors):
        self.name='togglebutton'
        self.render(colors)

    def _widget(self, state, color1, color2):
        if state is 'active':
            color2=gradient(color1,state)
            self._widget_bg(1,color1,color2,0,True)
        elif state is 'toggled':
            self._widget_bg(1,color1,color1)
        else:
            color2=gradient(color2,state)
            self._widget_bg(1,color1,color2,1,True)
    
    def render(self, colors):
        Button.render(self,colors)
        self.draw_asset('toggled', colors['border'].rgb, colors['bg'].rgb)

class Entry(Button):
    def __init__(self, colors):
        self.name='entry'
        self.render(colors)

    def _widget(self, state, color1, color2):
        self._bg()
        self._widget_bg(1, color1, color2, 1)

class ComboEntry(Entry):
    def __init__(self, colors):
        name=['combo-entry', 'combo-entry-button']
        for rotation, i in zip([2,0],[0,1]):
            self.name=name[i]
            self.render(colors, rotation)

    def _widget(self, state, color1, color2):
        self._bg()
        self.rounded_rectangle(color1, 19, 18, 0, 1, (3,3,0,0))
        self.rounded_rectangle(color2, 18, 16, 0, 2, (2,2,0,0))

class SpinButton(Entry):
    def __init__(self, colors):
        direction=['-up', '-down', '-down-rtl', '-up-rtl']
        for rotation in range(4):
            self.name='spinbutton'+direction[rotation]
            self.render(colors, rotation)

    def _widget(self, state, color1, color2):
        self._bg()
        self.rounded_rectangle(color1, 19, 19, 0, 1, (3,0,0,0))
        self.rounded_rectangle(color2, 18, 18, 0, 2, (2,0,0,0))

    def render(self, colors, rotation):
        self.draw_asset('normal', colors['border'].rgb, colors['bg'].rgb,rotation=rotation)
        self.draw_asset('disabled', colors['border'].disabled, colors['bg'].disabled,rotation=rotation)

class ScaleSlider(Button):
    def __init__(self, colors):
        self.name='scale-slider'
        self.render(colors)

    def _widget(self, state, color1, color2):
        color2=gradient(color2,'normal')
        if state in ['active', 'hover']:
            border=2
        else:
            border=1
        self.circle(color1, 10, 10, 9)
        self.circle(color2, 10, 10, 9-border, gradient=True)

class Arrow(Button):
    def __init__(self, colors):
        for self.scl in [1,2]:
            for name,size in zip(['arrow','arrow-small'],[12,8]):
                for rotation,direction in zip(range(4),['-up','-right','-down','-left']):
                    self.name=name+direction
                    self.render(colors,rotation,size,size)

    def _widget(self, state, color1, color2):
        if 'small' in self.name:
            self.arrow_small(color1)
        else:
            self.arrow(color1)

    def render(self, colors, rotation, w, h):
        self.draw_asset('normal', colors['fg'].rgb, None, rotation, w, h)
        self.draw_asset('disabled', colors['fg'].disabled, None, rotation, w, h)
        self.draw_asset('hover', colors['hover'].rgb, None, rotation, w, h)

class MenuArrow(Arrow):
    def __init__(self,colors1,colors2):
        self.name='menu-arrow'
        self.render(colors1,colors2)

    def render(self,colors1,colors2):
        self.draw_asset('normal',colors1['fg'].rgb,None,1,12,12)
        self.draw_asset('disabled',colors1['fg'].disabled,None,1,12,12)
        self.draw_asset('hover',colors2['selection_fg'].rgb,None,1,12,12)
        self.draw_asset('hover-disabled',colors2['selection_fg'].disabled,None,1,12,12)

class Scrollbar(Button):
    def __init__(self, colors):
        for self.scl in [1,2]:
            self.name='scrollbar-horizontal'
            self.render(colors,w=30,h=20)

            self.name='scrollbar-vertical'
            self.render(colors,w=20,h=30)

    def _widget(self, state, color1, color2):
        if self.name == 'scrollbar-horizontal':
            self.rounded_rectangle(color1,20,6,5,7,3)
        elif self.name == 'scrollbar-vertical':
            self.rounded_rectangle(color1,6,20,7,5,3)

    def render(self,colors,w,h):
        Button.render(self,colors,w=w,h=h)
        self.draw_asset('normal',mix(colors['fg'].rgb,Color.window_color,0.5),colors['bg'],w=w,h=h)
        self.draw_asset('focus', colors['active'].rgb,colors['bg'],w=w,h=h)

class TitleButtons(Button):
    def __init__(self, colors1, colors2):
        self.x=self.y=0
        for self.scl in [1,2]:
            for name in ['-close','-maximize','-maximize-maximized','-minimize','-stick', '-shade','-menu']:
                self.name='titlebutton'+name
                self.render(colors1,colors2)

    def _widget(self,state,color1,color2):
        if state in ['normal','inactive']  or (state=='active' and not 'title' in self.name):
            if 'close' in self.name:
                self.circle(color1,self.x+9,self.y+9,9)
                self.close(x=self.x,y=self.y)
            elif any(btn in self.name for btn in ['minimize','hide']):
                self.minimize(color1,x=self.x,y=self.y)
            elif 'maximized' in self.name:
                self.maximize_maximized(color1,x=self.x,y=self.y)
            elif 'stick' in self.name:
                self.maximize(color1,shifty=-2,x=self.x,y=self.y)
                self.maximize(color1,shifty=2,x=self.x,y=self.y)
            elif 'shade' in self.name:
                self.maximize(color1,shifty=2,x=self.x,y=self.y)
                self.rectangle(color1,self.x+4,self.y+5,10,1)
            elif 'menu' in self.name:
                self.rectangle(color1,self.x+4,self.y+4,10,2)
                self.rectangle(color1,self.x+4,self.y+8,10,2)
                self.rectangle(color1,self.x+4,self.y+12,10,2)
            else:
                self.maximize(color1,x=self.x,y=self.y)
        else:
            if 'close' in self.name:
                self.circle(color2,self.x+9,self.y+9,9)
                self.close(x=self.x,y=self.y)
            elif any(btn in self.name for btn in ['minimize','hide']):
                self.circle(color1,self.x+9,self.y+9,9)
                self.minimize(x=self.x,y=self.y)
            elif 'maximized' in self.name:
                self.circle(color1,self.x+9,self.y+9,9)
                self.maximize_maximized(x=self.x,y=self.y)
            elif 'stick' in self.name:
                self.circle(color1,self.x+9,self.y+9,9)
                self.maximize(None,shifty=-2,x=self.x,y=self.y)
                self.maximize(None,shifty=2,x=self.x,y=self.y)
            elif 'shade' in self.name:
                self.circle(color1,self.x+9,self.y+9,9)
                self.maximize(None,shifty=2,x=self.x,y=self.y)
                self.rectangle(None,self.x+4,self.y+5,10,1)
            elif 'menu' in self.name:
                self.circle(color1,self.x+9,self.y+9,9)
                self.rectangle(None,self.x+4,self.y+4,10,2)
                self.rectangle(None,self.x+4,self.y+8,10,2)
                self.rectangle(None,self.x+4,self.y+12,10,2)
            else:
                self.circle(color1,self.x+9,self.y+9,9)
                self.maximize(x=self.x,y=self.y)

    def render(self, colors1, colors2):
        self.draw_asset('normal',colors1['fg'].rgb,colors2['error'].rgb,w=18,h=18)
        self.draw_asset('hover', colors1['fg'].rgb,mix(colors2['error'].rgb,(1,1,1),0.5),w=18,h=18)
        self.draw_asset('active',mix(colors1['bg'].rgb,colors1['fg'].rgb,0.3),colors2['error'].rgb,w=18,h=18)
        self.draw_asset('inactive',colors1['inactive_fg'].rgb,colors2['error'].rgb,w=18,h=18)
        self.draw_asset('inactive-hover',colors1['inactive_fg'].rgb,mix(colors2['error'].rgb,(1,1,1),0.5),w=18,h=18)


class Checkbox(Drawing):
    n=5
    dn=1.0/n
    def __init__(self, colors):
        self.name='check'
        for self.scl in [1,2]:
            self.render(colors)

    def _widget(self, x, step, action, color, margin=0):
        x=x+margin
        Drawing.__init__(self,20+2*margin,20+2*margin, self.scl)
        self.rounded_rectangle('shadow',16,16,2+margin,2+margin,3)
        self.rounded_rectangle(color,16,16,x,x,3)
        self.rounded_rectangle(Color.window_color,14,14,x+1,x+1,2)

        size=10
        radius=1
        x=y=x+3

        if action=='check':
            delta=step*self.dn
        elif action=='uncheck':
            delta=1-step*self.dn

        if delta==0:
            return

        phi=(math.pi/4)*delta

        self.cr.new_sub_path()
        self.cr.arc(x+1, y+size-1, radius, (3*math.pi/4)-phi, (3*math.pi/4)+phi)
        self.cr.arc(x+((1-delta)*size)/2+1, y+((1-delta)*size)/2+1, radius, (5*math.pi/4)-phi, (5*math.pi/4)+phi)
        self.cr.arc(x+size-1, y+1, radius, (7*math.pi/4)-phi, (7*math.pi/4)+phi)
        self.cr.arc(x+((1+delta)*size)/2-1, y+((1+delta)*size)/2-1, radius, (math.pi/4)-phi, (math.pi/4)+phi)
        self.cr.close_path()
        self.cr.set_source_rgba(color[0],color[1],color[2],1.0)
        self.cr.fill()

    def mixed(self, x, state, color):
        self._widget(x+1,0,'uncheck',color)
        self.rounded_triangle(Color.window_color,6,6,6+x,6+x,1)
        self.save(self.name+'-mixed-'+state,self.scl)

    def unchecked(self, state, color):
        self._widget(2,0,'check',color)
        self.save(self.name+'-unchecked-'+state,self.scl)
        if self.name == 'check':
            self._widget(2,0,'check',color,10)
            self.save(self.name+'-selectionmode'+'-unchecked-'+state,self.scl)

    def checked(self, state, color):
        self._widget(2,0,'uncheck',color)
        self.save(self.name+'-checked-'+state,self.scl)
        if self.name == 'check':
            self._widget(2,0,'uncheck',color,10)
            self.save(self.name+'-selectionmode'+'-checked-'+state,self.scl)

    def animation(self, color):
        for action in ['check', 'uncheck']:
            for step in range(0,self.n+1):
                x=3-step*self.dn
                self._widget(x,step,action,color)  
                self.save(self.name+'-'+action+'-'+str(step),self.scl)             
                # if self.name == 'check':
                #     self._widget(x,step,action,color,10)
                #     self.save(self.name+'-'+action+'-selectionmode'+str(step),self.scl)
                
    def render(self, colors):
        self.unchecked('normal', colors['check_border'].rgb)
        self.checked('normal', colors['active'].rgb)
        self.mixed(1,'normal', colors['active'].rgb)
        self.unchecked('hover', colors['hover'].rgb)
        self.checked('hover', colors['hover'].rgb)
        self.mixed(1,'hover', colors['hover'].rgb)
        self.mixed(2,'hover-pressed', colors['hover'].rgb)
        self.unchecked('disabled', colors['check_border'].disabled)
        self.checked('disabled', colors['check_border'].disabled)
        self.mixed(1,'disabled', colors['check_border'].disabled)
        self.animation(colors['hover'].rgb)

class Radio(Checkbox):
    def __init__(self, colors):
        self.name='radio'
        for self.scl in [1,2]:
            self.render(colors)

    def _widget(self, x, step, action, color, margin=0):
        x=y=x+8
        Drawing.__init__(self,20+2*margin,20+2*margin)
        self.circle('shadow', 11, 11, 8)
        self.circle(color, x, y, 8)
        self.circle(Color.window_color, x, y, 7)

        if action=='check':
            delta=step*Checkbox.dn
        elif action=='uncheck':
            delta=1#-step*Checkbox.dn

        if delta==0:
            return

        radius=5

        x1=radius*math.cos((5*math.pi)/4)
        y1=radius*math.sin((5*math.pi)/4)

        x2=radius*math.cos(math.pi/4)
        y2=radius*math.sin(math.pi/4)

        r=radius/delta

        d=math.sqrt((x2-x1)**2 + (y2-y1)**2)
        x3=(x1+x2)/2
        y3=(y1+y2)/2
        x_c1=x3+math.sqrt(r**2-(d/2)**2)*(y1-y2)/d
        y_c1=y3+math.sqrt(r**2-(d/2)**2)*(x2-x1)/d
        x_c2=x3-math.sqrt(r**2-(d/2)**2)*(y1-y2)/d
        y_c2=y3-math.sqrt(r**2-(d/2)**2)*(x2-x1)/d

        phi=math.acos((((x1+x_c1)*(x2+x_c1))+((y1-y_c1)*(y2-y_c1)))/r**2)/2
        
        self.cr.new_sub_path()
        self.cr.arc(x_c1+x, -y_c1+y, r, (math.pi/4)-phi, (math.pi/4)+phi)
        self.cr.arc(x_c2+x, -y_c2+y, r, (5*math.pi/4)-phi, (5*math.pi/4)+phi)
        self.cr.close_path()
        self.cr.set_source_rgba(color[0],color[1],color[2],1.0)
        self.cr.fill()

    def mixed(self, x, state, color):
        self._widget(x+1,0,'uncheck',color)
        self.half_circle(Color.window_color,9+x,9+x,4)
        self.save(self.name+'-mixed-'+state,self.scl)

def troughs(color1,color2):
    for scl in [1,2]:
        scroll_h=Drawing(30,20,scl)
        scroll_h.rounded_rectangle(color1,20,6,5,7,3)
        scroll_h.save('scrollbar-trough-horizontal',scl)

        scroll_v=Drawing(20,30,scl)
        scroll_v.rounded_rectangle(color1,6,20,7,5,3)
        scroll_v.save('scrollbar-trough-vertical',scl)

    for rotation,direction in zip([0,1],['-horizontal', '-vertical']):
        progress=Drawing(10,10,rotation=rotation)
        progress.rounded_rectangle(color1,8,8,1,1,3)
        progress.save('progressbar'+direction+'-normal')

        progress=Drawing(8,8,rotation=rotation)
        progress.rounded_rectangle(color2,8,8,0,0,3)
        progress.save('progressbar'+direction+'-active')

        for state,color in zip(['-normal','-active'],[color1,color2]):
        
            scale=Drawing(20,20,rotation=rotation)
            scale.rounded_rectangle(color,6,6,7,7,3)
            scale.save('scale-trough'+direction+state)

def entry_background(color):
    entry = Drawing(20,20)
    entry.rectangle(color.rgb,0,0,20,20)
    entry.save('entry-background-normal')

    entry = Drawing(20,20)
    entry.rectangle(color.disabled,0,0,20,20)
    entry.save('entry-background-disabled')

def mixed_assets(color1, color2, color3):
    toolbar = Drawing(20,20)
    toolbar.save('null')
    toolbar.rectangle(color2,0,0,20,20)
    toolbar.save('toolbar-background')

    # Frame
    frame = Drawing(20,20)
    frame.rounded_rectangle(color1,20,20,0,0,0)
    frame.rounded_rectangle(color2,18,18,1,1,0)
    frame.save('frame')

        # Frame
    frame = Drawing(20,20)
    frame.rounded_rectangle(color1,20,20,0,0,3)
    frame.rounded_rectangle(color2,18,18,1,1,2)
    frame.save('menu-frame')

    # Tree header
    header = Drawing(20,20)
    header.rounded_rectangle(color1,20,20,0,0,0)
    header.rounded_rectangle(color2,19,19,0,0,0)
    header.save('tree-header')

    # Notebook gap
    notebook_gap = Drawing(4,2)
    notebook_gap.rectangle(color2,1,0,2,2)
    notebook_gap.save('notebook-gap-horizontal')

    notebook_gap = Drawing(2,4)
    notebook_gap.rectangle(color2,0,1,2,2)
    notebook_gap.save('notebook-gap-vertical')

    # Notebook frame
    direction = ['-top','-right','-bottom','-left']
    for i in range(4):
        notebook_frame = Drawing(20,20,rotation=i)
        notebook_frame.rounded_rectangle(color1,20,20,0,0,(3,3,3,0))
        notebook_frame.rounded_rectangle(color2,18,18,1,1,(2,2,2,0))
        notebook_frame.save('notebook-frame'+direction[i])

        tab = Drawing(20,20,rotation=i)
        tab.rounded_rectangle(color1,20,18,0,2,(3,0,0,3))
        tab.rounded_rectangle(color2,18,17,1,3,(2,0,0,2))
        tab.save('tab'+direction[i]+'-active')

        tab = Drawing(20,20,rotation=i)
        tab.rounded_rectangle(color3,20,20,0,0,(3,0,0,3), alpha=0.2)
        tab.save('tab'+direction[i]+'-inactive')

    # Frame gap
    frame_gap = Drawing(1,1)
    frame_gap.rectangle(color1,0,0,1,1)
    frame_gap.save('frame-gap')

    # frame_gap = Drawing(2,1)
    # frame_gap.rectangle(color1,0,0,1,1)
    # frame_gap.save('frame-gap-end')

    # Lines
    lines = Drawing(20,1)
    lines.rectangle(color1,0,0,20,1)
    lines.save('line-h')

    lines = Drawing(1,20)
    lines.rectangle(color1,0,0,1,20)
    lines.save('line-v')

    lines = Drawing(20,1)
    lines.rectangle(color2,0,0,20,1)
    lines.save('handle-h')

    lines = Drawing(1,20)
    lines.rectangle(color2,0,0,1,20)
    lines.save('handle-v')

def menubar(color):
    bar = Drawing(20,20)
    bar.rectangle(color,1,1,18,18)
    bar.save('menubar-button')

    bar = Drawing(20,20)
    bar.rectangle(mix(color, Color.window_color,0.2),1,1,18,18)
    bar.save('pathbar-button')

class Xfwm4(Button):
    def __init__(self,colors1,colors2):
        for i in range(1,6):
            self.name='title-'+str(i)
            self.render(colors1,colors2,5,26)
        for side in ['left','right']:
            self.name=side
            self.render(colors1,colors2,1,16)
            self.name='top-'+side
            self.render(colors1,colors2,1,26)
            self.name='bottom-'+side
            self.render(colors1,colors2,16,1)
        self.name='bottom'
        self.render(colors1,colors2,1,1)
        self.name='menu'
        self.render(colors1,colors2,20,26)

    def _widget(self,state,color1,color2):
        if any(s in self.name for s in ['title','top','menu']) and state=='active':
            color=gradient(lighter(color1,0.10),0,0,0,26)
            self.cr.set_source(color)
        elif any(s in self.name for s in ['title','top']) and state=='inactive':
            self.cr.set_source_rgb(color1[0],color1[1],color1[2])
        else:
            self.cr.set_source_rgb(color2[0],color2[1],color2[2])
        self.cr.paint()

    def render(self, colors1, colors2, w, h):
        self.draw_asset('active', colors1['bg'].rgb,colors2['bg'].rgb, w=w,h=h)
        self.draw_asset('inactive', colors1['inactive_bg'].rgb,colors2['bg'].rgb, w=w,h=h)

class Xfwm4Buttons(TitleButtons):
    def __init__(self,colors1,colors2):
        self.x=1; self.y=4
        for name in ['close','hide','maximize','stick','shade']:
            self.name=name
            self.render(colors1,colors2)

    def _bg(self,color):
        color=gradient(lighter(color,0.10),0,0,0,26)
        self.rectangle(color,0,0,20,27,True)
        self.cr.save()

    def _titlebutton(self,state,color1,color2):
        TitleButtons._widget(self,state,color1,color2)

    def _widget(self,state,color1,color2,color3):
        #self._bg(color1)
        self._titlebutton(state,color2,color3)

    def draw_asset(self, state, color1, color2, color3, rotation=0, w=20, h=20):
        Drawing.__init__(self,w,h,self.scl,rotation)
        self._widget(state, color1, color2, color3)
        bg=Drawing(w,h)
        bg.assetsdir=self.assetsdir
        if state == 'inactive':
            col=color1
            grad=False
        else:
            col=gradient(lighter(color1,0.10),0,0,0,h)
            grad=True
        bg.rectangle(col,0,0,20,26,grad)
        btn=cairo.SurfacePattern(self.surface)
        bg.cr.set_source(btn)
        bg.cr.paint()
        bg.save(self.name+'-'+state,self.scl)

    def render(self, colors1, colors2):
        self.draw_asset('active',colors1['bg'].rgb,colors1['fg'].rgb,colors2['error'].rgb,w=20,h=27)
        self.draw_asset('prelight',colors1['bg'].rgb, colors1['fg'].rgb,mix(colors2['error'].rgb,(1,1,1),0.5),w=20,h=27)
        self.draw_asset('pressed',colors1['bg'].rgb,mix(colors1['bg'].rgb,colors1['fg'].rgb,0.3),colors2['error'].rgb,w=20,h=27)
        self.draw_asset('inactive',colors1['inactive_bg'].rgb,colors1['inactive_fg'].rgb,mix(colors2['error'].rgb,(1,1,1),0.5),w=20,h=27)

#__________________________________________________________________________

def _read_globals(filename):
    _colors={}
    with open(filename, 'r') as _kde_globals:
        for widget in ['Disabled','Button','Selection','Tooltip','View','Window','WM']:
            for line in _kde_globals:
                if line.strip().split(':')[-1].strip('[]') == widget:
                    break
            for line in _kde_globals:
                if line == '\n':
                    break
                _colors['{0}{1}'.format(widget,line.strip().split('=')[0])] = line.strip().split('=')[1]
    return _colors

def _str_to_color(color):
    r,g,b=(float(color.split(',')[0])/255,float(color.split(',')[1])/255,float(color.split(',')[2])/255)
    return (r,g,b)

def _create_dirs(path):
    try:
        os.makedirs(path)
    except OSError as exception:
        if exception.errno != errno.EEXIST:
            raise

def _gtk_color(gtk_version,name,color):
    if gtk_version == 2:
        string='gtk-color-scheme = "{0}:#{1:02x}{2:02x}{3:02x}"\n'.format(name, int(round(color[0]*255)),int(round(color[1]*255)),int(round(color[2]*255)))
    elif gtk_version == 3:
        string='@define-color {0} rgb({1},{2},{3});\n'.format(name, int(round(color[0]*255)),int(round(color[1]*255)),int(round(color[2]*255)))
    return string

def main(color_file,outdir,create_links):
    assetsdir = os.path.join(outdir, 'assets/')
    gtk2dir = os.path.join(outdir, 'gtk-2.0/')
    gtk3dir = os.path.join(outdir, 'gtk-3.0/')
    xfwmdir = os.path.join(outdir, 'xfwm4/')

    Drawing.assetsdir=assetsdir
    kde_globals=_read_globals(color_file)
    # set disabled values
    try:
        Color.intensity_effect=int(kde_globals['DisabledIntensityEffect'])
        Color.intensity_amount=float(kde_globals['DisabledIntensityAmount'])
        Color.contrast_effect=int(kde_globals['DisabledContrastEffect'])
        Color.contrast_amount=float(kde_globals['DisabledContrastAmount'])
        Color.color_effect=int(kde_globals['DisabledColorEffect'])
        Color.color_amount=float(kde_globals['DisabledColorAmount'])
        Color.effect_color=_str_to_color(kde_globals['DisabledColor'])
    except KeyError:
        print('Could not find disabled values, using defaults')
        pass

    Color.window_color=_str_to_color(kde_globals['WindowBackgroundNormal'])
    
    button_colors={}
    button_colors['border']=Color(kde_globals['WindowBackgroundNormal'],kde_globals['WindowForegroundNormal'], 0.75)
    button_colors['check_border']=Color(kde_globals['WindowBackgroundNormal'],kde_globals['WindowForegroundNormal'],0.5)
    button_colors['bg']=Color(kde_globals['ButtonBackgroundNormal'])
    button_colors['hover']=Color(kde_globals['ButtonDecorationHover'])
    button_colors['active']=Color(kde_globals['ButtonDecorationFocus'])
    button_colors['fg']=Color(kde_globals['ButtonForegroundNormal'])

    view_colors={}
    view_colors['border']=button_colors['border']
    view_colors['bg']=Color(kde_globals['ViewBackgroundNormal'])
    view_colors['hover']=Color(kde_globals['ViewDecorationHover'])
    view_colors['active']=Color(kde_globals['ViewDecorationFocus'])
    view_colors['fg']=Color(kde_globals['ViewForegroundNormal'])

    window_colors={}
    window_colors['bg']=Color(kde_globals['WindowBackgroundNormal'])
    window_colors['fg']=Color(kde_globals['WindowForegroundNormal'])

    titlebar_colors={}
    titlebar_colors['bg']=Color(kde_globals['WMactiveBackground'])
    titlebar_colors['fg']=Color(kde_globals['WMactiveForeground'])
    titlebar_colors['inactive_bg']=Color(kde_globals['WMinactiveBackground'])
    titlebar_colors['inactive_fg']=Color(kde_globals['WMinactiveForeground'])

    other_colors={}
    other_colors['link']=Color(kde_globals['ViewForegroundLink'])
    other_colors['link_visited']=Color(kde_globals['ViewForegroundVisited'])
    other_colors['warning']=Color(kde_globals['ViewForegroundNeutral'])
    other_colors['error']=Color(kde_globals['ViewForegroundNegative'])
    other_colors['success']=Color(kde_globals['ViewForegroundPositive'])
    other_colors['trough']=Color(kde_globals['WindowForegroundNormal'],kde_globals['WindowBackgroundNormal'],0.3)
    other_colors['scrollbar']=mix(other_colors['trough'].rgb,view_colors['fg'].rgb,0.5)
    other_colors['tooltip_fg']=Color(kde_globals['TooltipForegroundNormal'])
    other_colors['tooltip_bg']=Color(kde_globals['TooltipBackgroundNormal'])
    other_colors['selection_bg']=Color(kde_globals['SelectionBackgroundNormal'])
    other_colors['selection_fg']=Color(kde_globals['SelectionForegroundNormal'])
    other_colors['selection_hover']=Color(kde_globals['SelectionDecorationHover'])

    if create_links:
        if os.path.exists('/usr/share/themes/Breeze'):
            if not os.path.exists(os.path.join(gtk2dir,'widgets')):
                os.symlink('/usr/share/themes/Breeze/gtk-2.0/widgets', os.path.join(gtk2dir,'widgets'))
            if not os.path.exists(os.path.join(gtk3dir,'gtk-main.css')):
                os.symlink('/usr/share/themes/Breeze/gtk-3.0/gtk-main.css', os.path.join(gtk3dir,'gtk-main.css'))
            #fix_config(homedir)
        else:
            print('Breeze-gtk is not installed')
            sys.exit()

    _create_dirs(assetsdir)
    _create_dirs(gtk2dir)
    _create_dirs(gtk3dir)

    Button(button_colors).draw_asset('focus', button_colors['active'].rgb, button_colors['active'].rgb)
    #Button().draw_asset('active', button_colors['active'].rgb, button_colors['active'].rgb)
    ToolButton(button_colors)
    ToggleButton(button_colors)
    ScaleSlider(button_colors)
    Arrow(button_colors)
    Scrollbar(button_colors)
    Entry(view_colors)
    ComboEntry(view_colors)
    SpinButton(view_colors)
    Checkbox(button_colors)
    Radio(button_colors)
    MenuArrow(window_colors,other_colors)
    TitleButtons(titlebar_colors,other_colors)
    troughs(other_colors['trough'].rgb,other_colors['selection_bg'].rgb)
    mixed_assets(button_colors['border'].rgb,window_colors['bg'].rgb,window_colors['fg'].rgb)
    menubar(other_colors['selection_bg'].rgb)
    entry_background(view_colors['bg'])

    # _create_dirs(xfwmdir)
    # Xfwm4.assetsdir=xfwmdir
    # Xfwm4(titlebar_colors,window_colors)
    # Xfwm4Buttons.assetsdir=xfwmdir
    # Xfwm4Buttons(titlebar_colors,other_colors)

    with open(os.path.join(gtk2dir,'gtkrc'), 'w') as gtkrc:
        gtkrc.writelines(
            ['# Theme:       Breeze-gtk\n',
            '# Description: Breeze theme for GTK+2.0\n',
            '\n',
            _gtk_color(2, 'text_color', view_colors['fg'].rgb),
            _gtk_color(2, 'base_color', view_colors['bg'].rgb),
            _gtk_color(2, 'insensitive_base_color',view_colors['bg'].disabled),
            _gtk_color(2, 'fg_color', window_colors['fg'].rgb),
            _gtk_color(2, 'bg_color', window_colors['bg'].rgb),
            _gtk_color(2, 'selected_fg_color', other_colors['selection_fg'].rgb),
            _gtk_color(2, 'selected_bg_color', other_colors['selection_bg'].rgb),
            _gtk_color(2, 'button_fg_color', button_colors['fg'].rgb),
            _gtk_color(2, 'tooltip_fg_color', other_colors['tooltip_fg'].rgb),
            _gtk_color(2, 'tooltip_bg_color', other_colors['tooltip_bg'].rgb),
            _gtk_color(2, 'insensitive_fg_color', window_colors['fg'].disabled),
            _gtk_color(2, 'insensitive_text_color', view_colors['fg'].disabled),
            _gtk_color(2, 'button_insensitive_fg_color', button_colors['fg'].disabled),
            _gtk_color(2, 'button_active', button_colors['active'].rgb),
            _gtk_color(2, 'border_color', button_colors['border'].rgb),
            _gtk_color(2, 'link_color', other_colors['link'].rgb),
            _gtk_color(2, 'link_visited', other_colors['link_visited'].rgb),
            _gtk_color(2, 'menu_color', window_colors['bg'].rgb),
            '\n',
            'include "widgets/main.rc"\n'#,
            # 'include "widgets/buttons"\n',
            # 'include "widgets/menu"\n',
            # 'include "widgets/entry"\n',
            # 'include "widgets/notebook"\n',
            # 'include "widgets/range"\n',
            # 'include "widgets/scrollbar"\n',
            # 'include "widgets/toolbar"\n',
            # 'include "widgets/progressbar"\n',
            # 'include "widgets/misc"\n',
            # 'include "widgets/styles"\n'
            ]
            )
        gtkrc.close()

    with open(os.path.join(gtk3dir,'gtk.css'), 'w') as gtk3rc:
        gtk3rc.writelines(
            ['\n/* Button Colors */\n',
            _gtk_color(3, 'button_bg_a', lighter(button_colors['bg'].rgb,0.01)),
            _gtk_color(3, 'button_bg_b', darker(button_colors['bg'].rgb,0.03)),
            _gtk_color(3, 'button_bg_color', button_colors['bg'].rgb),
            _gtk_color(3, 'button_dis_bg_color', button_colors['bg'].disabled),
            _gtk_color(3, 'button_fg', button_colors['fg'].rgb),
            _gtk_color(3, 'button_hover', button_colors['hover'].rgb),
            _gtk_color(3, 'button_active', button_colors['active'].rgb),
            _gtk_color(3, 'button_active_bg_a', lighter(button_colors['active'].rgb,0.03)),
            _gtk_color(3, 'button_active_bg_b', darker(button_colors['active'].rgb,0.10)),
            _gtk_color(3, 'button_active_hover_a',lighter(button_colors['hover'].rgb,0.03)),
            _gtk_color(3, 'button_active_hover_b',darker(button_colors['hover'].rgb,0.10)),
            _gtk_color(3, 'button_dis_fg', button_colors['fg'].disabled),
            _gtk_color(3, 'button_dis_bg_a', lighter(button_colors['bg'].disabled,0.01)),
            _gtk_color(3, 'button_dis_bg_b', darker(button_colors['bg'].disabled,0.03)),
            '\n/* Selection Colors */\n',
            _gtk_color(3, 'selection_bg', other_colors['selection_bg'].rgb),
            _gtk_color(3, 'selection_fg', other_colors['selection_fg'].rgb),
            _gtk_color(3, 'selection_hover', other_colors['selection_hover'].rgb),
            '\n/* View Colors */\n',
            _gtk_color(3, 'view_bg', view_colors['bg'].rgb),
            _gtk_color(3, 'view_fg', view_colors['fg'].rgb),
            _gtk_color(3, 'view_hover', view_colors['hover'].rgb),
            _gtk_color(3, 'view_active',view_colors['active'].rgb),
            _gtk_color(3, 'view_dis_bg', view_colors['bg'].disabled),
            _gtk_color(3, 'view_dis_fg', view_colors['fg'].disabled),
            '\n/* Window Colors */\n',
            _gtk_color(3, 'window_bg', window_colors['bg'].rgb),
            _gtk_color(3, 'window_fg', window_colors['fg'].rgb),
            _gtk_color(3, 'window_dis_fg', window_colors['fg'].disabled),
            '\n/* Titlebar Colors */\n',
            _gtk_color(3, 'titlebar_bg_a', lighter(titlebar_colors['bg'].rgb,0.10)),
            _gtk_color(3, 'titlebar_bg_b', titlebar_colors['bg'].rgb),
            _gtk_color(3, 'titlebar_fg', titlebar_colors['fg'].rgb),
            _gtk_color(3, 'titlebar_bd_bg', titlebar_colors['inactive_bg'].rgb),
            _gtk_color(3, 'titlebar_bd_fg', titlebar_colors['inactive_fg'].rgb),
            _gtk_color(3, 'titlebar_dis_fg', titlebar_colors['fg'].disabled),
            '\n/* Tooltip Colors */\n',
            _gtk_color(3, 'tooltip_bg', other_colors['tooltip_bg'].rgb),
            _gtk_color(3, 'tooltip_fg', other_colors['tooltip_fg'].rgb),
            '\n/* Border Colors */\n',
            _gtk_color(3, 'borders', button_colors['border'].rgb),
            _gtk_color(3, 'borders_dis', button_colors['border'].disabled),
            '\n/* Other Colors */\n',
            _gtk_color(3, 'menu_color', window_colors['bg'].rgb),
            _gtk_color(3, 'link_color', other_colors['link'].rgb),
            _gtk_color(3, 'link_visited', other_colors['link_visited'].rgb),
            _gtk_color(3, 'warning_color', other_colors['warning'].rgb),
            _gtk_color(3, 'error_color', other_colors['error'].rgb),
            _gtk_color(3, 'success_color', other_colors['success'].rgb),
            _gtk_color(3, 'trough_color', other_colors['trough'].rgb),
            _gtk_color(3, 'trough_dis_color', other_colors['trough'].disabled),
            _gtk_color(3, 'scrollbar_color', other_colors['scrollbar']),
            '\n/* WM Colors*/\n',
            _gtk_color(3, 'wm_title', titlebar_colors['fg'].rgb),
            _gtk_color(3, 'wm_unfocused_title', titlebar_colors['inactive_fg'].rgb),
            _gtk_color(3, 'wm_bg', titlebar_colors['bg'].rgb),
            _gtk_color(3, 'wm_unfocused_bg', titlebar_colors['inactive_bg'].rgb),
            _gtk_color(3, 'wm_highlight', other_colors['selection_bg'].rgb),
            _gtk_color(3, 'wm_button_close_bg', titlebar_colors['fg'].rgb),
            _gtk_color(3, 'wm_button_close_hover_bg', mix(other_colors['error'].rgb,(1,1,1),0.5)),
            _gtk_color(3, 'wm_button_close_active_bg', other_colors['error'].rgb),
            _gtk_color(3, 'wm_button_close_bg_unfocused', titlebar_colors['inactive_fg'].rgb),
            _gtk_color(3, 'wm_button_bg', titlebar_colors['fg'].rgb),
            _gtk_color(3, 'wm_button_active_bg',mix(titlebar_colors['bg'].rgb,titlebar_colors['fg'].rgb,0.3)),
            _gtk_color(3, 'wm_button_bg_unfocused', titlebar_colors['inactive_fg'].rgb),
            '\n@import url("gtk-main.css");']
            )
        gtk3rc.close()

        # with open(os.path.join(xfwmdir,'themerc'), 'w') as xfwmrc:
        #     xfwmrc.writelines(
        #         ['button_offset=2\n',
        #         'button_spacing=4\n',
        #         'show_app_icon=true\n',
        #         'full_width_title=true\n',
        #         'title_shadow_active=false\n',
        #         'title_shadow_inactive=false\n',
        #         'title_horizontal_offset=3\n',
        #         'active_text_color={0}\n'.format(titlebar_colors['fg'].html),
        #         'inactive_text_color={0}\n'.format(titlebar_colors['inactive_fg'].html),
        #         'shadow_delta_height=2\n',
        #         'shadow_delta_width=0\n',
        #         'shadow_delta_x=0\n',
        #         'shadow_delta_y=-5\n',
        #         'shadow_opacity=40\n']
        #         )

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--input", help="path to colorfile")
    parser.add_argument("-o", "--outputdir", help="path to outputdir")
    parser.add_argument("-f", "--force", help="create assets even if theme is not installed", action="store_true")
    args = parser.parse_args()

    homedir = os.path.expanduser('~')
    color_file = os.path.join(homedir,".config/kdeglobals")
    outdir = os.path.join(homedir,".themes/")
    create_links = True

    if args.input:
        if os.path.exists(args.input):
            color_file = args.input
        else:
            print("couldn't find {0}".format(color_file))
            sys.exit()
        
    if args.outputdir:
        if os.path.exists(args.outputdir):
            outdir = args.outputdir
        else:
            print('path does not exist')
            sys.exit()

    if args.force:
        create_links = False

    if os.geteuid() == 0:
        if not args.input:
            print('specify colorfile by using \"breeze-gtk-colors -i {path_to_file}\"')
            sys.exit()
        else:
            outdir = '/usr/share/themes/'
            create_links = False

    main(color_file,outdir,create_links)




