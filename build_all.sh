#!/bin/bash

colors=( Breeze BreezeDark )
for f in ./color-schemes/*.colors
do
	i=${f%.colors}
	i=${i##*/}
	if [[ $i != *"Breeze"* ]]; then
	i="Breeze"$i
	fi
	echo $i

	if [ -d ./$i ]
	  then rm -rf ./$i
	fi
	
	mkdir ./$i
	python render_assets.py -f -i $f -o ./$i

	mkdir ./$i/gtk-3.18
	mkdir ./$i/gtk-3.20

	cp ./$i/gtk-3.0/gtk.css ./$i/gtk-3.18/gtk.css
	cp ./$i/gtk-3.0/gtk.css ./$i/gtk-3.20/gtk.css
	rm ./$i/gtk-3.0/gtk.css
    
    if [ "$i" = "BreezeDark" ]; then
        sassc src/gtk318/gtk-main-dark.scss ./$i/gtk-3.18/gtk-main.css 
        sassc src/gtk320/gtk-main-dark.scss ./$i/gtk-3.20/gtk-main.css
	else
        sassc src/gtk318/gtk-main.scss ./$i/gtk-3.18/gtk-main.css 
        sassc src/gtk320/gtk-main.scss ./$i/gtk-3.20/gtk-main.css 
    fi
	  
	cp -R src/gtk2/widgets $i/gtk-2.0/
done

