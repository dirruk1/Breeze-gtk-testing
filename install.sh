INSTALL_DIR="/usr/share/themes/Breeze"
THEME_DIR="Breeze"

if [ "$#" -ne 1 ]
then
  echo "specify gtk version"
  exit 1
fi

if [ -d $INSTALL_DIR ]
  then  sudo rm -rf $INSTALL_DIR
fi

sudo mkdir $INSTALL_DIR
sudo cp -R $THEME_DIR/gtk-2.0  $INSTALL_DIR/gtk-2.0
sudo cp -R $THEME_DIR/assets  $INSTALL_DIR/assets
#sudo cp -R $THEME_DIR/xfwm4  $INSTALL_DIR/xfwm4
sudo cp -R $THEME_DIR/metacity-1  $INSTALL_DIR/metacity-1
sudo cp $THEME_DIR/index.theme $INSTALL_DIR/index.theme

case "$1" in
    3.16)   sudo cp -R $THEME_DIR/gtk-3.16 $INSTALL_DIR/gtk-3.0
            ;;
    3.18)   sudo cp -R $THEME_DIR/gtk-3.18 $INSTALL_DIR/gtk-3.0
            ;;
    3.20)   sudo cp -R $THEME_DIR/gtk-3.20 $INSTALL_DIR/gtk-3.0
            ;;
    *)      echo "invalid option"
esac

#chmod 755 apply-gtk-colors
#sudo cp apply-gtk-colors /usr/bin/
